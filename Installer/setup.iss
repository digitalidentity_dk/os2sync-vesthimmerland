; This file is a script that allows to build the instalation package
; The script may be executed from the console-mode compiler - iscc "c:\isetup\samples\my script.iss" or from the Inno Setup Compiler UI
#define AppId "{{4ad2221a-e13a-431d-adea-f70da7575add}"
#define AppSourceDir "..\OS2syncVesthimmerland\bin\Publish"
#define AppName "OS2syncVesthimmerland"
#define AppVersion "4.6.2"
#define AppPublisher "Digital Identity"
#define AppURL "http://digital-identity.dk/"

[Setup]
AppId={#AppId}
AppName={#AppName}
AppVersion={#AppVersion}
AppPublisher={#AppPublisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={pf}\{#AppPublisher}\{#AppName}
DefaultGroupName={#AppName}
DisableProgramGroupPage=yes
OutputBaseFilename={#AppName}-{#AppVersion}
Compression=lzma
SolidCompression=yes
SourceDir= {#SourcePath}\{#AppSourceDir}
OutputDir={#SourcePath}

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "*.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "*.json"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "*.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "mssql\*"; DestDir: "{app}\mssql"; Flags: ignoreversion recursesubdirs

[Run]
Filename: "{app}\OS2syncVesthimmerland.exe"; Parameters: "install" 

[UninstallRun]
Filename: "{app}\OS2syncVesthimmerland.exe"; Parameters: "uninstall"
