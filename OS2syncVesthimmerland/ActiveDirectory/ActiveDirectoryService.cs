﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Text;

namespace OS2syncVesthimmerland
{
    class ADAndStatus
    {
        public string Uuid { get; set; }
        public string MobilePhone { get;set; }
        public string HomePhone { get; set; }
        public bool Enabled { get; set; }
    }

    class ActiveDirectoryService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ADAndStatus GetUuid(string sAMAccountName)
        {
            using (var ctx = new PrincipalContext(ContextType.Domain))
            {
                using (var user = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, sAMAccountName))
                {
                    if (user != null)
                    {
                        bool enabled = ((bool)((user.Enabled != null) ? user.Enabled : true));
                        string mobilePhone = null;
                        string homePhone = null;

                        try
                        {
                            using (DirectoryEntry de = user.GetUnderlyingObject() as DirectoryEntry)
                            {
                                if (de != null)
                                {
                                    mobilePhone = de.Properties["mobile"].Value as string;
                                    homePhone = de.Properties["homePhone"].Value as string;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Warn("Failed to lookup phone data in AD for " + sAMAccountName + ", error = " + ex.Message);
                        }

                        return new ADAndStatus
                        {
                            Enabled = enabled,
                            HomePhone = homePhone,
                            MobilePhone = mobilePhone,
                            Uuid = user.Guid.ToString().ToLower()
                        };
                    }
                }
            }

            return null;
        }
    }
}
