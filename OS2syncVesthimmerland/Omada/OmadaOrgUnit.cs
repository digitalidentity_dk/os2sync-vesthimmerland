﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OS2syncVesthimmerland
{
    class OmadaOrgUnit
    {
        public long Id { get; set; }
        public string ChangeTime { get; set; }
        public string UId { get; set; }  // used as UUID
        public string NAME { get; set; }
        public string PHONE { get; set; }
        public string ADDRESS1 { get; set; }
        public string ZIPCODE { get; set; }
        public string CITY { get; set; }
        public OmadaOrgUnitReference PARENTOU { get; set; }
        public List<OmadaUserReference> MANAGER { get; set; }
    }
}
