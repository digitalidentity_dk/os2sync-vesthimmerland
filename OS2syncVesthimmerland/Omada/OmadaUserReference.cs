﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OS2syncVesthimmerland
{
    class OmadaUserReference
    {
        public long Id { get; set; } // supplied by Omada

        public string Uuid { get; set; } // not supplied by Omada, we will fill it out later through AD lookup (first we fetch the sAMAccountName from Omada, then exchange to UUID in AD)
    }
}
