﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OS2syncVesthimmerland
{
    class OmadaUser
    {
        public long Id { get; set; }
        public string Uuid { get; set; } // not supplied by Omada, we will fill it out later by lookup in AD
        public bool Enabled { get; set; } // not supplied by Omada, we will fill it out later by lookup in AD
        public string HomePhone { get; set; } // not supplied by Omada, we will fill it out later by lookup in AD
        public string MobilePhone { get; set; } // not supplied by Omada, we will fill it out later by lookup in AD
        public string ChangeTime { get; set; } // anvendes som synkroniseringsmekanisme
        public string DisplayName { get; set; } // dette er den som VHK gerne vil bruge som navn (lige nu)
        public string FIRSTNAME { get; set; } // jeg hiver dem med ud, men bruger dem ikke (endnu)
        public string LASTNAME { get; set; } // jeg hiver dem med ud, men bruger dem ikke (endnu)
        public string IDENTITYID { get; set; } // sAMAccountName
        public string EMAIL { get; set; }
        public string JOBTITLE { get; set; }
        public string PHONE { get; set; }
        public string VHK_CPR { get; set; } // er ikke udfyldt i Omada, så skal hentes fra AD efterfølgende
        public OmadaIdentityStatus IDENTITYSTATUS { get; set; }
        public List<OmadaOrgUnitReference> VHK_ORGANIZATIONAL_UNITS { get; set; }
    }
}
