﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OS2syncVesthimmerland
{
    class OmadaOrgUnitReference
    {
        public long Id { get; set; } // this is supplied by Omada, we will use it to lookup and find the Uuid

        public string Uuid { get; set; } // not supplied by Omada, we will fill it out ourselves by an actual lookup
    }
}
