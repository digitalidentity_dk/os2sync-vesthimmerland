﻿using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OS2syncVesthimmerland
{
    class OmadaService
    {
        private static ActiveDirectoryService activeDirectoryService = new ActiveDirectoryService();
        private static string username = Configuration.Get("Omada:username");
        private static string password = Configuration.Get("Omada:password");
        private static List<long> filteredOus = Configuration.GetLongs("Omada:filteredOUs");

        public List<OmadaUser> GetAllUsers(string timestamp, List<OmadaOrgUnit> orgUnits)
        {
            RestClient client = new RestClient("https://brugerportalen.vesthimmerland.dk");
            client.Authenticator = new NtlmAuthenticator(username, password);

            var request = new RestSharp.RestRequest("/OData/DataObjects/Identity", Method.GET);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddQueryParameter(@"$select", "Id,Uid,ChangeTime,DisplayName,FIRSTNAME,LASTNAME,IDENTITYID,EMAIL,JOBTITLE,PHONE,VHK_CPR,VHK_ORGANIZATIONAL_UNITS,IDENTITYSTATUS");
            if (!string.IsNullOrEmpty(timestamp))
            {
                request.AddQueryParameter(@"$filter", "ChangeTime gt " + timestamp);
            }

            var response = client.Execute<OmadaUserWrapper>(request);
            var result = response.Data.value;

            // filter out invalid users
            result.RemoveAll(u => string.IsNullOrEmpty(u.IDENTITYID) ||
                                  string.IsNullOrEmpty(u.FIRSTNAME) ||
                                  string.IsNullOrEmpty(u.LASTNAME) ||
                                  u.VHK_ORGANIZATIONAL_UNITS == null ||
                                  u.VHK_ORGANIZATIONAL_UNITS.Count == 0);

            // map to OrgUnit
            foreach (var user in result)
            {
                foreach (var ouRef in user.VHK_ORGANIZATIONAL_UNITS)
                {
                    foreach (var ou in orgUnits)
                    {
                        if (ou.Id == ouRef.Id)
                        {
                            ouRef.Uuid = ou.UId;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        public List<OmadaOrgUnit> GetAllOrgUnits(/*string timestamp*/)
        {
            RestClient client = new RestClient("https://brugerportalen.vesthimmerland.dk");
            client.Authenticator = new NtlmAuthenticator(username, password);

            var request = new RestSharp.RestRequest("/OData/DataObjects/Orgunit", Method.GET);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddQueryParameter(@"$select", "Id,UId,ChangeTime,NAME,MANAGER,PHONE,PARENTOU,ZIPCODE,ADDRESS1,CITY");

            // this actually works, but we cannot $expand, so...
            //            request.AddQueryParameter(@"$filter", "ChangeTime gt 2020-09-01T15:00:15.873+02:00");

            // TODO: this does not work, so I guess we will simple read ALL, and then handle filtering later
            //            request.AddQueryParameter(@"$expand", "PARENTOU(UId)");


            var response = client.Execute<OmadaOrgUnitWrapper>(request);
            var result = response.Data.value;

            // remove extra root OUs
            result.RemoveAll(o => filteredOus.Contains(o.Id));

            // expand Uid
            foreach (var ou in result)
            {
                if (ou.PARENTOU == null)
                {
                    ou.PARENTOU = new OmadaOrgUnitReference();
                    ou.PARENTOU.Uuid = null;

                    continue;
                }

                foreach (var parent in result)
                {
                    if (ou.PARENTOU.Id == parent.Id)
                    {
                        ou.PARENTOU.Uuid = parent.UId;
                        break;
                    }
                }
            }

            // TODO: cannot expand MANAGER, and individual lookup is to extreme...

            return result;
        }
    }
}
