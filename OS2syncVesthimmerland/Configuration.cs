﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;

namespace OS2syncVesthimmerland
{
    class Configuration
    {
        private static IConfigurationRoot configuration;

        static Configuration()
        {
            configuration = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json")
             .AddEnvironmentVariables()
             .Build();
        }

        public static string Get(string key)
        {
            return configuration[key];
        }

        public static List<long> GetLongs(string key)
        {
            return configuration.GetSection(key).Get<List<long>>();
        }
    }
}
