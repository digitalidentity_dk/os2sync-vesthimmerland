﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;

namespace OS2syncVesthimmerland
{
    [DisallowConcurrentExecution]
    [PersistJobDataAfterExecution]
    public class SynchronizeJob : IJob
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string userTimestamp, orgUnitTimestamp;
        private static OmadaService omadaService = new OmadaService();
        private static ActiveDirectoryService activeDirectoryService = new ActiveDirectoryService();
        private static OS2Service os2service = new OS2Service();

        private void LoadState(IJobExecutionContext context)
        {
            if (userTimestamp == null)
            {
                userTimestamp = (string)context.JobDetail.JobDataMap["DATA_USER_TIMESTAMP"];
            }

            if (orgUnitTimestamp == null)
            {
                orgUnitTimestamp = (string)context.JobDetail.JobDataMap["DATA_ORGUNIT_TIMESTAMP"];
            }
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                LoadState(context);

                // hvis state er null (dvs vi aldrig har kaldt før), så hent alt fra OMADA
                List<OmadaOrgUnit> orgUnits = omadaService.GetAllOrgUnits(/*orgUnitTimestamp*/);
                List<OmadaUser> users = omadaService.GetAllUsers(userTimestamp, orgUnits);

                // TODO: this only works when orgUnitTimestamp is not supplied above
                long roots = orgUnits.Count(o => o.PARENTOU == null || string.IsNullOrEmpty(o.PARENTOU.Uuid));
                if (roots != 1)
                {
                    throw new Exception("No single OrgUnit with NULL parent. Found " + roots + " root OrgUnits: " + string.Join(",", orgUnits.Where(o => string.IsNullOrEmpty(o.PARENTOU?.Uuid)).Select(o => o.Id).ToArray()));
                }

                // filter out all OrgUnits that has not changed since last time we ran
                orgUnits.RemoveAll(o => o.ChangeTime.CompareTo(orgUnitTimestamp) < 0);

                if (orgUnits.Count > 0)
                {
                    try
                    {
                        // update timestamps
                        foreach (OmadaOrgUnit orgUnit in orgUnits)
                        {
                            if (orgUnitTimestamp != null)
                            {
                                if (orgUnit.ChangeTime.CompareTo(orgUnitTimestamp) > 0)
                                {
                                    orgUnitTimestamp = orgUnit.ChangeTime;
                                }
                            }
                            else
                            {
                                orgUnitTimestamp = orgUnit.ChangeTime;
                            }

                            // send to OS2sync
                            os2service.Save(orgUnit);
                        }

                        // finally update timestamp if all went well
                        context.JobDetail.JobDataMap.Put("DATA_ORGUNIT_TIMESTAMP", orgUnitTimestamp);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to synchronize orgUnits from Omada to OS2sync.", ex);
                    }
                }

                if (users.Count > 0)
                {
                    try
                    {
                        // update timestamps
                        foreach (OmadaUser user in users)
                        {
                            if (userTimestamp != null)
                            {
                                if (user.ChangeTime.CompareTo(userTimestamp) > 0)
                                {
                                    userTimestamp = user.ChangeTime;
                                }
                            }
                            else
                            {
                                userTimestamp = user.ChangeTime;
                            }

                            // fetch UUID from AD
                            var res = activeDirectoryService.GetUuid(user.IDENTITYID);
                            if (res != null)
                            {
                                user.Uuid = res.Uuid;
                                // TODO: would be nice to read from Omada instead
                                user.Enabled = res.Enabled;
                                user.MobilePhone = res.MobilePhone;
                                user.HomePhone = res.HomePhone;
                            }

                            if (!string.IsNullOrEmpty(user.Uuid))
                            {
                                // send to OS2sync
                                os2service.Save(user);
                            }
                        }

                        // finally update timestamp if all went well
                        context.JobDetail.JobDataMap.Put("DATA_USER_TIMESTAMP", userTimestamp);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to synchronize users from Omada to OS2sync.", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Omada Synchronization Failed.", ex);
            }

            return Task.CompletedTask;
        }
    }
}

