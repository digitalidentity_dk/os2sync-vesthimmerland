﻿using Organisation.BusinessLayer.DTO.Registration;
using Organisation.SchedulingLayer;
using System;

namespace OS2syncVesthimmerland
{
    class OS2Service
    {
        private UserDao userDao = new UserDao();
        private OrgUnitDao orgUnitDao = new OrgUnitDao();

        public void Save(OmadaUser user)
        {
            UserRegistration reg = new UserRegistration();
            reg.Uuid = user.Uuid;
            reg.UserId = user.IDENTITYID;
            reg.Person.Name = user.FIRSTNAME + " " + user.LASTNAME;

            // TODO: this requires manual testing before we deploy, as it will delete anyone without this set
            var operation = OperationType.UPDATE;
            if (!user.Enabled)
            {
                operation = OperationType.DELETE;
            }

            /* does not work
            if (!"a4ed4124-9b94-43a0-92c0-e71fba733049".Equals(user.IDENTITYSTATUS?.Uid))
            {
                operation = OperationType.DELETE;
            }
            */

            if (!string.IsNullOrEmpty(user.EMAIL))
            {
                reg.Email = user.EMAIL;
            }


            if (!string.IsNullOrEmpty(user.HomePhone))
            {
                reg.Landline = "9966" + user.HomePhone;
            }
            else if (!string.IsNullOrEmpty(user.PHONE))
            {
                reg.Landline = user.PHONE;
            }

            if (!string.IsNullOrEmpty(user.MobilePhone))
            {
                reg.PhoneNumber = user.MobilePhone;
            }

            if (!string.IsNullOrEmpty(user.VHK_CPR))
            {
                reg.Person.Cpr = user.VHK_CPR;
            }

            foreach (var p in user.VHK_ORGANIZATIONAL_UNITS)
            {
                if (!string.IsNullOrEmpty(p.Uuid))
                {
                    reg.Positions.Add(new Position()
                    {
                        Name = user.JOBTITLE,
                        OrgUnitUuid = p.Uuid,
                        StartDate = DateTime.Now.ToString("yyyy-MM-dd"),
                        StopDate = null
                    });
                }
            }

            if (reg.Positions.Count > 0)
            {
                userDao.Save(reg, operation, false, 10, Configuration.Get("Cvr"));
            }
        }

        public void Save(OmadaOrgUnit orgUnit)
        {
            OrgUnitRegistration reg = new OrgUnitRegistration();
            reg.Uuid = orgUnit.UId;
            reg.Name = orgUnit.NAME;

            // TODO: test this
            var operation = OperationType.UPDATE;
            if (orgUnit.NAME.ToLower().StartsWith("kt.:") || orgUnit.NAME.ToLower().StartsWith("ophørt"))
            {
                operation = OperationType.DELETE;
            }

            if (!string.IsNullOrEmpty(orgUnit.ADDRESS1) && !string.IsNullOrEmpty(orgUnit.CITY) && !string.IsNullOrEmpty(orgUnit.ZIPCODE))
            {
                reg.Post = orgUnit.ADDRESS1 + ", " + orgUnit.ZIPCODE + " " + orgUnit.CITY;
            }

            if (orgUnit.PARENTOU != null && !string.IsNullOrEmpty(orgUnit.PARENTOU.Uuid))
            {
                reg.ParentOrgUnitUuid = orgUnit.PARENTOU.Uuid;
            }

            if (!string.IsNullOrEmpty(orgUnit.PHONE))
            {
                reg.PhoneNumber = orgUnit.PHONE;
            }

            orgUnitDao.Save(reg, operation, false, 10, Configuration.Get("Cvr"));
        }
    }
}
