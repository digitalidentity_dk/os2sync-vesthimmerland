﻿using System;
using System.Runtime.InteropServices;
using Topshelf;
using Topshelf.Runtime.DotNetCore;

namespace OS2syncVesthimmerland
{
    class Program
    {
        public static void Main(String[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<WindowsService>();
                x.SetServiceName("OS2sync Omada Listener");
                x.SetDisplayName("OS2sync Omada Listener");
                x.SetDescription("Acts as a bridge between Omada and OS2sync");

                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    x.UseEnvironmentBuilder(new Topshelf.HostConfigurators.EnvironmentBuilderFactory(c =>
                    {
                        return new DotNetCoreEnvironmentBuilder(c);
                    }));
                }
            });
        }
    }
}
